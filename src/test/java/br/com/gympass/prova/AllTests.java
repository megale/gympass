package br.com.gympass.prova;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.com.gympass.prova.controller.ControladorTest;
import br.com.gympass.prova.file.ArquivoTest;

@RunWith(Suite.class)
@SuiteClasses({ ArquivoTest.class, ControladorTest.class })
public class AllTests {

}
