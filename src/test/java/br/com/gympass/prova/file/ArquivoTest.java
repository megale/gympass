package br.com.gympass.prova.file;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import br.com.gympass.prova.model.Log;

public class ArquivoTest {

	@Test
	public void testSize() throws IOException {

		Arquivo arquivo = new Arquivo();
		List<Log> logs = arquivo.lerArquivo();
		assertEquals(23, logs.size());
		
	}
	
	@Test
	public void testFirst() throws IOException {

		Arquivo arquivo = new Arquivo();
		List<Log> logs = arquivo.lerArquivo();
		assertEquals("23:49:08.277", logs.get(0).getHora().toString());
		assertEquals("038 – F.MASSA", logs.get(0).getPiloto());
		assertEquals(new Integer(1), logs.get(0).getVolta());
		assertEquals("00:01:02.852", logs.get(0).getTempo().toString());
		assertEquals("44.275", logs.get(0).getMedia().toString());
		
	}
	
	@Test
	public void testLast() throws IOException {

		Arquivo arquivo = new Arquivo();
		List<Log> logs = arquivo.lerArquivo();
		assertEquals("23:54:57.757", logs.get(logs.size()-1).getHora().toString());
		assertEquals("011 – S.VETTEL", logs.get(logs.size()-1).getPiloto());
		assertEquals(new Integer(3), logs.get(logs.size()-1).getVolta());
		assertEquals("00:01:18.097", logs.get(logs.size()-1).getTempo().toString());
		assertEquals("35.633", logs.get(logs.size()-1).getMedia().toString());

	}

}