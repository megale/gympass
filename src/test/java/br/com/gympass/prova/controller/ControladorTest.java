package br.com.gympass.prova.controller;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import br.com.gympass.prova.model.Piloto;

public class ControladorTest {

	private static List<Piloto> pilotos;

	@BeforeClass
	public static void init() throws IOException {
		Controlador controlador = new Controlador();
		pilotos = controlador.start();
	}

	@Test
	public void testPilotos() throws IOException {

		assertEquals(6, pilotos.size());
		assertEquals("038 – F.MASSA", pilotos.get(0).getNome());
		assertEquals("011 – S.VETTEL", pilotos.get(5).getNome());

	}

	@Test
	public void testQuantidadeDeVoltas() throws IOException {

		assertEquals(4, pilotos.get(0).getVoltas().intValue());
		assertEquals(3, pilotos.get(5).getVoltas().intValue());

	}

	@Test
	public void testTempoTotalDeProva() throws IOException {

		assertEquals("00:04:11.578", pilotos.get(0).getTempo().toString());
		assertEquals("00:06:27.276", pilotos.get(5).getTempo().toString());

	}

	@Test
	public void testPosicaoDeChegada() throws IOException {

		assertEquals(1, pilotos.get(0).getPosicao().intValue());
		assertEquals(6, pilotos.get(5).getPosicao().intValue());

	}

	@Test
	public void testMelhorVolta() throws IOException {

		assertEquals("00:01:02.769", pilotos.get(0).getMelhorVolta().toString());
		assertEquals("00:01:18.097", pilotos.get(5).getMelhorVolta().toString());

	}

	@Test
	public void testMediaDeVelocidade() throws IOException {

		assertEquals("44,246 KM/h", pilotos.get(0).getVelocidadeMedia());
		assertEquals("25,746 KM/h", pilotos.get(5).getVelocidadeMedia());

	}

	@Test
	public void testTempoAtrasDoPrimeiro() throws IOException {

		assertEquals("00:00", pilotos.get(0).getTempoAtras().toString());
		assertEquals("00:02:15.698", pilotos.get(5).getTempoAtras().toString());

	}

}