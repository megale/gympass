package br.com.gympass.prova.model;

import java.time.LocalTime;

import br.com.gympass.prova.view.Coluna;

public class Piloto {

	private String nome;

	private Integer voltas;

	private LocalTime tempo;

	private Integer posicao;
	
	private LocalTime melhorVolta;
	
	private String velocidadeMedia;
	
	private LocalTime tempoAtras;

	public Piloto(String nome) {
		this.nome = nome;
		this.voltas = 0;
		this.tempo = LocalTime.of(00, 00);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Piloto other = (Piloto) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Coluna(nome = "Voltas", posicao = 2)
	public Integer getVoltas() {
		return voltas;
	}

	public void setVoltas(Integer voltas) {
		this.voltas = voltas;
	}

	@Coluna(nome = "Tempo total", posicao = 3)
	public LocalTime getTempo() {
		return tempo;
	}

	public void setTempo(LocalTime tempo) {
		this.tempo = tempo;
	}

	@Coluna(nome = "Posição", posicao = 0)
	public Integer getPosicao() {
		return posicao;
	}

	public void setPosicao(Integer posicao) {
		this.posicao = posicao;
	}

	@Coluna(nome = "Piloto", posicao = 1)
	public String getNome() {
		return nome;
	}
	
	public void incrementarVolta() {
		this.voltas++;
	}
	
	public void somarTempo(LocalTime tempo) {
		this.tempo = this.tempo.plusHours(tempo.getHour()).plusMinutes(tempo.getMinute()).plusSeconds(tempo.getSecond()).plusNanos(tempo.getNano());
	}

	@Coluna(nome = "Melhor Volta", posicao = 4)
	public LocalTime getMelhorVolta() {
		return melhorVolta;
	}

	public void setMelhorVolta(LocalTime melhorVolta) {
		this.melhorVolta = melhorVolta;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Coluna(nome = "Velocidade Média", posicao = 5)
	public String getVelocidadeMedia() {
		return velocidadeMedia;
	}

	public void setVelocidadeMedia(String velocidadeMedia) {
		this.velocidadeMedia = velocidadeMedia;
	}

	@Coluna(nome = "Tempo após", posicao = 6)
	public LocalTime getTempoAtras() {
		return tempoAtras;
	}

	public void setTempoAtras(LocalTime melhorTempo) {
		this.tempoAtras = tempo.minusHours(melhorTempo.getHour()).minusMinutes(melhorTempo.getMinute()).minusSeconds(melhorTempo.getSecond()).minusNanos(melhorTempo.getNano());
	}
	
}