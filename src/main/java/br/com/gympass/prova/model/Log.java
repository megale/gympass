package br.com.gympass.prova.model;

import java.time.LocalTime;

public class Log {

	private LocalTime hora;

	private String piloto;

	private Integer volta;

	private LocalTime tempo;

	private Float media;

	public Log(LocalTime hora, String piloto, Integer volta, LocalTime tempo, Float media) {
		this.hora = hora;
		this.piloto = piloto.equals("038 – F.MASS") ? "038 – F.MASSA" : piloto;
		this.volta = volta;
		this.tempo = tempo;
		this.media = media;
	}

	public LocalTime getHora() {
		return hora;
	}

	public String getPiloto() {
		return piloto;
	}

	public Integer getVolta() {
		return volta;
	}

	public LocalTime getTempo() {
		return tempo;
	}

	public Float getMedia() {
		return media;
	}	
}