package br.com.gympass.prova.file;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import br.com.gympass.prova.model.Log;

public class Arquivo {

	private Stream<String> linhas;

	public List<Log> lerArquivo() throws IOException {

		// Ler arquivo de log
		Path caminho = Paths.get("arquivo.log");
		linhas = Files.lines(caminho, StandardCharsets.UTF_8);
		List<String> conteudo = linhas.collect(Collectors.toList());
		
		// Remover cabeçalho
		conteudo.remove(0);

		// Hora
		List<LocalTime> horas = getHora(conteudo);
		
		// Piloto
		List<String> pilotos = getPilotos(conteudo);
		
		// Volta
		List<Integer> voltas = getVoltas(conteudo);
		
		// Tempo da Volta
		List<LocalTime> tempos = getTempos(conteudo);
		
		// Velocidade média
		List<Float> medias = getVelocidadeMedia(conteudo);
		
		List<Log> logs = new ArrayList<>();
		for (int i = 0; i < horas.size(); i++) {
			logs.add(new Log(horas.get(i), pilotos.get(i), voltas.get(i), tempos.get(i), medias.get(i)));
		}
		
		return logs;
		
	}

	private List<LocalTime> getHora(List<String> conteudo) {
		
		Pattern patternHora = Pattern.compile("([0-9]{2}:[0-9]{2}:[0-9]{2}\\.[0-9]{3})");
		Stream<String> horasStream = conteudo.stream().map(linha -> veryficarCorrenpondencia(patternHora, linha));
		
		List<LocalTime> horas = new ArrayList<>();
		horasStream.forEach(tempo -> horas.add(LocalTime.parse(tempo, DateTimeFormatter.ofPattern("HH:mm:ss.SSS"))));
		
		return horas;
		
	}

	private List<String> getPilotos(List<String> conteudo) {
		
		Pattern patternPiloto = Pattern.compile("(([\\d]{3}) . ([\\w]\\.[\\w]+))");
		List<String> pilotos = conteudo.stream().map(linha -> veryficarCorrenpondencia(patternPiloto, linha)).collect(Collectors.toList());

		return pilotos;
		
	}

	private List<Integer> getVoltas(List<String> conteudo) {
		
		Pattern patternVolta = Pattern.compile("([^\\d][\\d][^\\d])");
		Stream<String> voltasStream = conteudo.stream().map(linha -> veryficarCorrenpondencia(patternVolta, linha));
		
		List<Integer> voltas = new ArrayList<>();
		voltasStream.forEach(volta -> voltas.add(Integer.parseInt(volta)));
		
		return voltas;
		
	}

	private List<LocalTime> getTempos(List<String> conteudo) {
		
		Pattern patternTempo = Pattern.compile("([^\\d]\\d:\\d\\d.[\\d]{3})");
		Stream<String> temposStream = conteudo.stream().map(linha -> veryficarCorrenpondencia(patternTempo, linha));
		
		List<LocalTime> tempos = new ArrayList<>();
		temposStream.forEach(tempo -> tempos.add(LocalTime.parse("00:0"+tempo, DateTimeFormatter.ofPattern("HH:mm:ss.SSS"))));
		
		return tempos;
		
	}

	private List<Float> getVelocidadeMedia(List<String> conteudo) {
		
		Pattern patternMedia = Pattern.compile("([\\d]{2},[\\d]+)");
		Stream<String> mediasStream = conteudo.stream().map(linha -> veryficarCorrenpondencia(patternMedia, linha));
		
		List<Float> medias = new ArrayList<>();
		mediasStream.forEach(media -> medias.add(Float.parseFloat(media.replace(",", "."))));
		
		return medias;
		
	}

	private String veryficarCorrenpondencia(Pattern pattern, String linha) {

		Matcher matcher = pattern.matcher(linha);
		return matcher.find() ? matcher.group(1).trim() : "";

	}
}