package br.com.gympass.prova.view;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import br.com.gympass.prova.model.Piloto;

public class Janela extends JFrame {

	private static final long serialVersionUID = -222052806639399481L;
	
	private JPanel contentPane;
	private JTable table;

	/**
	 * Create the frame.
	 */
	public Janela(List<Piloto> lista) {

		setTitle("Resultado da corrida");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1300, 254);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		ModeloDaTabela modeloDaTabela = new ModeloDaTabela(lista);

		table = new JTable();
		table.setModel(modeloDaTabela);
		table.setFont(new Font("Serif", Font.PLAIN, 20));
		table.getColumnModel().getColumn(0).setWidth(50);
		table.getColumnModel().getColumn(1).setPreferredWidth(250);
		table.getColumnModel().getColumn(2).setPreferredWidth(50);
		table.getColumnModel().getColumn(3).setPreferredWidth(150);
		table.getColumnModel().getColumn(4).setPreferredWidth(150);
		table.getColumnModel().getColumn(5).setPreferredWidth(150);
		scrollPane.setViewportView(table);

	}
}