package br.com.gympass.prova.controller;

import java.awt.EventQueue;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import br.com.gympass.prova.file.Arquivo;
import br.com.gympass.prova.model.Log;
import br.com.gympass.prova.model.Piloto;
import br.com.gympass.prova.view.Janela;

public class Controlador {

	public List<Piloto> start() throws IOException {

		// Realizar a leitura do log (Corrida)
		List<Log> corrida = new Arquivo().lerArquivo();
		
		// Criar lista de pilotos
		Set<Piloto> pilotosSet = new HashSet<>();
		corrida.forEach(volta -> pilotosSet.add(new Piloto(volta.getPiloto())));
		List<Piloto> pilotos = new ArrayList<>(pilotosSet);
		
		// Realizar número de voltas e tempo
		corrida.forEach(volta -> pilotos.forEach(piloto -> calcular(volta, piloto)));

		// Classificar por tempo (crescente)
		pilotos.sort(Comparator.comparing(Piloto::getTempo));
		
		// Classificar por número de voltas (decrescente)
		pilotos.sort(Comparator.comparing(Piloto::getVoltas).reversed()); 
		
		// Preencher posição de chegada
		for (int i = 0; i < pilotos.size(); i++)
			pilotos.get(i).setPosicao(i+1);

		// Calcular melhor volta de casa piloto
		pilotos.forEach(piloto -> calcularMelhorVolta(piloto, corrida));
		
		// Calcular velocidade média de casa piloto
		pilotos.forEach(piloto -> calcularVelocidadeMedia(piloto, corrida));
		
		// Tempo após
		LocalTime melhorTempo = pilotos.get(0).getTempo();
		pilotos.forEach(piloto -> piloto.setTempoAtras(melhorTempo));
		
		// Exibir resultados
		shoeResults(pilotos);

		return pilotos;
		
	}


	private void calcularVelocidadeMedia(Piloto piloto, List<Log> corrida) {

		// Somar velocidades
		double velocidadeSomadas = corrida.stream().filter(c -> c.getPiloto().equals(piloto.getNome())).mapToDouble(Log::getMedia).sum();
		
		// Calcular
		Double v = velocidadeSomadas / piloto.getVoltas();
		
		// Alterar precisão da resposta
		String velocidadeFormatada = new DecimalFormat("0.###").format(v);
		
		// Set valor
		piloto.setVelocidadeMedia(velocidadeFormatada + " KM/h");
		
	}

	private void calcularMelhorVolta(Piloto piloto, List<Log> corrida) {
		
		Optional<Log> min = corrida.stream().filter(c -> c.getPiloto().equals(piloto.getNome())).min(Comparator.comparing(Log::getTempo));
		piloto.setMelhorVolta(min.get().getTempo());

	}

	private void calcular(Log volta, Piloto piloto) {
		
		if (volta.getPiloto().equals(piloto.getNome())) {
			piloto.incrementarVolta();
			piloto.somarTempo(volta.getTempo());
		}
		
	}
	private void shoeResults(List<Piloto> pilotos) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Janela frame = new Janela(pilotos);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}
	

	public static void main(String[] args) {

		try {
			new Controlador().start();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}